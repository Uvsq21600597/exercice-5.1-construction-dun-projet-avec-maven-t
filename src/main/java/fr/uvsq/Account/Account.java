package fr.uvsq.Account;



public class Account {

	private double solde;
	
	
	public Account(double solde) {
		
		if (solde >0){
			this.solde=solde;
		}
		
		else{
			throw new ArithmeticException("l'initialisation de compte doit �tre avec un montant positif");
		}}
	
	
	
	public double CheckAccount(){
			
			return this.solde;
	}
	
	

	public double Debit (double value) {
		
	if( value>0 ){
		
				if(this.solde>value) return this.solde=this.solde-value;
				else throw new ArithmeticException("Vous n'avez plus d'argent sur votre compte pour effectuer cette op�ration");
				}
	else throw new ArithmeticException("�chec de l'autorisation de paiement");
	
	}
	
	
	public double Credit (double value) {
		
		if(value>0) {
			return this.solde=this.solde+value ;
		}
		else throw new ArithmeticException("la valeur ajoute� au solde doit �tre plus de zero ");
	}
	
	
   public void transfer (double value,Account c){
	
		this.Debit(value);
		c.Credit(value);	
		
}}